package bibleReader;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import bibleReader.model.BibleReaderModel;
import bibleReader.model.NavigableResults;
import bibleReader.model.Reference;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * The display panel for the Bible Reader.
 * 
 * @author cusack
 * @editedBy Nazareth Frezghi and Carmen Rodriguez, 2019
 */
public class ResultView extends JPanel {

	// Fields
	public JScrollPane scroll;
	public JEditorPane resultPane;
	BibleReaderModel model;
	int resultsFound;
	String searchWord;
	ArrayList<Reference> refs;
	VerseList verses;
	public BibleReaderApp bibleApp;
	public NavigableResults navResults;
	public boolean able = true;
	// public JButton nextButton;
	// public JButton previousButton;
	// int pageNumber;
	// int totalPages;

	/**
	 * Construct a new ResultView and set its model to myModel. It needs to
	 * model to look things up.
	 * 
	 * @param myModel
	 *            The model this view will access to get information.
	 */
	public ResultView(BibleReaderModel myModel) {
		// sets the model to the passed model
		model = myModel;

		// creates a new JTextPane where the results will be displayed
		resultPane = new JEditorPane();
		resultPane.setName("OutputEditorPane");

		// creates a new JScrollPane
		scroll = new JScrollPane(resultPane);

		// sets the result pane to not be editable and the content type to
		// text/html so it can display the provided html
		resultPane.setEditable(false);
		resultPane.setContentType("text/html");
		add(scroll, BorderLayout.CENTER);

	}

	/**
	 * Gives the scrollPane
	 * 
	 * @return Returns the JScrollPane from the ResultView
	 */
	public JScrollPane getScroll() {
		return scroll;
	}

	// Handles the setting of the HTML table in the results view
	public void setWordSearchResults(NavigableResults nav) {
		navResults = nav;
		updateSearchResults();

	}

	public void updateSearchResults() {
		// Initializes the stringBufferer
		StringBuffer sb = new StringBuffer();
		// Creates a new VerseList
		verses = new VerseList("", "");

		// Create a local String variable called Phrase that will help us bold
		// the
		// searchword
		String phrase = "";

		// sets the result count to the size of the currentResults in the
		// navigable
		// results
		int resultCount = navResults.currentResults().size();

		// If you have at least 1 result, print the HTML String
		if (resultCount > 0) {
			// Add the strings onto the StringBuffer
			sb.append("<html><head><title>Bible Results</title></head><body>");
			sb.append("<table><tr><td valign='top' width='100'>Verse</td>");

			// For all of the Versions of the Bible in model, print the
			// abbreviation of the bible at the top
			String[] versionList = model.getVersions();
			for (int i = 0; i < versionList.length; i++) {
				sb.append("<td>" + versionList[i] + "</td>");
			}
			sb.append("</tr>");
			// for every reference, print the reference in a column
			for (Reference r : navResults.currentResults()) {
				sb.append("<tr><td valign='top'>" + r.toString() + "</td>");
				// for every verse, bold the word being searched in the results
				for (String version : versionList) {
					String phrase1 = model.getText(version, r);
					String word = navResults.getQueryPhrase();
					phrase = phrase1.replaceAll ("(?i)(?<!\\w)" + word + "(?!\\w)", "<b>$0</b>");
//					phrase = phrase1.replaceAll("(?i)" + word, "<b>$0</b>");
					sb.append("<td>" + phrase + "</td>");
				}

				sb.append("</tr>");
			}

			// Add the string to end the HTML table
			sb.append("</table></body></html>");

			// Sets the text of the Pane to the HTML Table
			resultPane.setText(sb.toString());
			resultPane.setCaretPosition(0);

			// Sets the scrollbar and the result pane to visible
			resultPane.setVisible(true);
			scroll.setVisible(true);

		} else if (resultCount == 0) { // If there are no results, then print no
										// results in the pane
			resultPane.setText("No Results Found");
			resultPane.setVisible(true);
		}

		// Adds the enabled and disabled features to the buttons.
		if (!navResults.hasNextResults()) {
			// bibleApp.nextButton.setEnabled(false);
			able = false;
		}
		if (navResults.hasNextResults()) {
			// bibleApp.nextButton.setEnabled(true);
			able = true;
		}
		if (!navResults.hasPreviousResults()) {
			// bibleApp.previousButton.setEnabled(false);
			able = false;
		}
		if (navResults.hasPreviousResults()) {
			// bibleApp.previousButton.setEnabled(true);
			able = true;
		}
	}

	// Handles the setting of the HTML table in the results view
	public void setPassageResults(NavigableResults nav) {
		navResults = nav;
		updatePassageResults();
	}

	public void updatePassageResults() {
		// Initialize the StringBuffer
		StringBuffer sb = new StringBuffer();
		// Creates a new VerseList
		verses = new VerseList("", "");

		// sets the result count to the size of the currentResults in
		// navigableResults
		int resultCount = navResults.currentResults().size();

		// If you have more than 1 result, print the HTML String
		if (resultCount > 0) {
			// Creates two references to be able to get the first verse number
			// from one and
			// the last verse number from the other
			// to later use these to create a reference title for the passage
			// being
			// searched.
			Reference ref1 = navResults.currentResults().get(0);
			Reference ref2 = navResults.currentResults().get(navResults.currentResults().size() - 1);

			// Add the strings onto the StringBuffer
			sb.append("<html><head><title>Bible Results</title></head><body>");
			// (ex: displays John2:3-2:12 as John 2:3-12).
			if (ref1.getChapter() == ref2.getChapter()) {
				sb.append("<center><b>" + ref1.toString() + "-" + ref2.getVerse() + "</b> </center>");
			} else { // if the chapters are different, include the chapter
						// number
				sb.append("<center><b>" + ref1.toString() + "-" + ref2.getChapter() + ":" + ref2.getVerse()
						+ "</b> </center>");
			}

			sb.append("<table><tr><td valign='top'>");

			// For all of the Versions of the Bible in model, print the
			// abbreviation of the bible at the top
			String[] versionList = model.getVersions();
			for (int i = 0; i < versionList.length; i++) {
				sb.append(versionList[i] + "</td>");
			}
			sb.append("</tr><tr>");
			for (String version : versionList) {
				sb.append("<td valign='top'><p>");

				for (Reference r : navResults.currentResults()) {

					// if (r.getVerse() != 1 && r.getChapter() == 1) {
					if (r.getVerse() != 1) {
						if (!model.getText(version, r).equals("")) {
							sb.append("<sup>" + r.getVerse() + "</sup>");
						}
					} else {
						sb.append("</p><p></p>");
						sb.append("<sup>" + r.getChapter() + "</sup>");
					}
					sb.append(model.getText(version, r));

				}
				sb.append("</td>");

			}
			sb.append("</tr>");

			// Add the string to end the HTML table
			sb.append("</table></body></html>");

			// Sets the text of the Pane to the HTML Table
			resultPane.setText(sb.toString());
			resultPane.setCaretPosition(0);

			// Sets the scrollbar and the result pane to visible
			resultPane.setVisible(true);
			scroll.setVisible(true);

		} else if (resultCount == 0) { // If there are no results, then print no
										// results in the pane
			resultPane.setText("No Results Found");
			resultPane.setVisible(true);
		}

		// Adds the enabled and disabled features to the buttons.
		if (!navResults.hasNextResults()) {
			// bibleApp.nextButton.setEnabled(false);
			able = false;
		}
		if (navResults.hasNextResults()) {
			// bibleApp.nextButton.setEnabled(true);
			able = true;
		}
		if (!navResults.hasPreviousResults()) {
			// bibleApp.previousButton.setEnabled(false);
			able = false;
		}
		if (navResults.hasPreviousResults()) {
			// bibleApp.previousButton.setEnabled(true);
			able = true;
		}
	}

	/**
	 * Returns the size of the references search so that the BibleReaderApp
	 * class can display how many search results there were.
	 * 
	 * @return the size of the ArrayList of References holding the search
	 *         results
	 */
	public int getNumberOfResults(NavigableResults nav) {
		return nav.currentResults().size();
	}

	/**
	 * Returns true or false whether the buttons are able or not.
	 * 
	 * @return a boolean that shows the state of the buttons
	 */
	public boolean getAble() {
		return able;
	}

}