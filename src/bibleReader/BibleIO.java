package bibleReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import bibleReader.model.Bible;
import bibleReader.model.BookOfBible;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * A utility class that has useful methods to read/write Bibles and Verses.
 * 
 * @author Cusack
 * @editedBy Nazareth Frezghi
 */
public class BibleIO {

	/**
	 * Read in a file and create a Bible object from it and return it.
	 * 
	 * @param bibleFile
	 * @return
	 */
	public static VerseList readBible(File bibleFile) {
		String name = bibleFile.getName();
		String extension = name.substring(name.lastIndexOf('.') + 1, name.length());

		if ("atv".equals(extension.toLowerCase())) {
			return readATV(bibleFile);
		} else if ("xmv".equals(extension.toLowerCase())) {
			return readXMV(bibleFile);

		} else {
			return null;
		}
	}

	/**
	 * Read in a Bible that is saved in the "ATV" format. The format is
	 * described below.The first line is a summary of what is in the file. In
	 * the case of a Bible, the first line will be of the following form.
	 * ABBREVIATION: FULL TITLE where ABBREVIATION is generally the acronym used
	 * (e.g. KJV for the King James Version), and FULL TITLE is the full title
	 * of the version. Each remaining line is of the form:
	 * BOOK@CHAPTER:VERSE@TEXT For instance, here is Genesis 1:19 from the kjv
	 * file: Ge@1:19@And the evening and the morning were the fourth day.
	 * 
	 * @param bibleFile
	 *            The file containing a Bible with .atv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if
	 *         there was an error reading the file.
	 */
	private static VerseList readATV(File bibleFile) {

		VerseList toReturn = new VerseList("string", "string");
		Verse v;
		String version;
		String title;

		try {
			BufferedReader br = new BufferedReader(new FileReader(bibleFile));
			String line1 = br.readLine();
			if (line1.isEmpty()) {
				version = "unknown";
				title = "";
			} else {
				if (line1.contains(": ")) {
					String[] firstln = line1.split(": ", 2);

					version = firstln[0];
					title = firstln[1];
				} else {

					version = line1;
					title = "";
				}

			}
			ArrayList<Verse> catalog = new ArrayList<Verse>();

			String line;
			while ((line = br.readLine()) != null) {
				if (line.contains("@")) {
					String[] temp = line.split("@", 2);
					if (temp[1].contains(":")) {
						String[] temp2 = temp[1].split(":", 2);
						if (temp2[1].contains("@")) {
							String[] temp3 = temp2[1].split("@", 2);

							String bookAbbreviation = temp[0];
							BookOfBible bookBible = BookOfBible.getBookOfBible(bookAbbreviation);
							if (bookBible == null) {
								br.close();
								return null;
							}
							int chapter = Integer.parseInt(temp2[0]);
							int vers = Integer.parseInt(temp3[0]);
							String text = temp3[1];

							v = new Verse(bookBible, chapter, vers, text);
							catalog.add(v);

						}
					}
				} else {
					br.close();
					return null;
				}
			}
			toReturn = new VerseList(version, title, catalog);

		} catch (IOException e) {
			e.getMessage();
			return null;
		}
		return toReturn;

	}

	/**
	 * Read in the Bible that is stored in the XMV format.
	 * 
	 * @param bibleFile
	 *            The file containing a Bible with .xmv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if
	 *         there was an error reading the file.
	 */
	private static VerseList readXMV(File bibleFile) {
		VerseList toReturn = new VerseList("string", "string");
		Verse v = new Verse(null, 0, 0, "");
		String version;
		String title;
		String bookAb = "";
		BookOfBible bookOfBible = null;
		int chapt = 0;
		int vers = 0;
		String text = "";

		try {
			BufferedReader br = new BufferedReader(new FileReader(bibleFile));
			String line1 = br.readLine();
			if (line1.isEmpty()) {
				version = "unknown";
				title = "";
			} else {
				if (line1.contains(": ")) {
					String[] firstln = line1.split(": ", 2);
					String[] f = firstln[0].split(" ");
					version = f[1];
					title = firstln[1];
				} else {
					version = line1;
					title = "";
				}
			}
			ArrayList<Verse> catalog = new ArrayList<Verse>();

			String line;
			while ((line = br.readLine()) != null) {
				line.trim();
				if (line.startsWith("<Book")) {

					String[] book = line.split(",");
					String[] book2 = book[0].split(" ", 2);
					bookAb = book2[1];
					bookOfBible = BookOfBible.getBookOfBible(bookAb);
				}
				if (line.startsWith("<Chapter")) {
					String[] chapter = line.split(" ");
					String chapter2 = chapter[1].replace(">", "");

					try {
						chapt = Integer.parseInt(chapter2);
					} catch (NumberFormatException numFE) {
						numFE.getStackTrace();
					}
				}
				if (line.startsWith("<Verse")) {

					String verse = line.replace(">", " ");
					String[] verse2 = verse.split(" ", 3);

					vers = Integer.parseInt(verse2[1]);

					text = verse2[2].trim();
					v = new Verse(bookOfBible, chapt, vers, text);
					catalog.add(v);
				}
			}
			br.close();
			toReturn = new VerseList(version, title, catalog);
		} catch (IOException e) {
			e.getMessage();
			return null;
		}
		return toReturn;
	}

	/**
	 * Write out the Bible in the ATV format.
	 * 
	 * @param file
	 *            The file that the Bible should be written to.
	 * @param bible
	 *            The Bible that will be written to the file.
	 */
	public static void writeBibleATV(File file, Bible bible) {
		try {
			FileWriter outputStream = new FileWriter(file);
			PrintWriter pW = new PrintWriter(outputStream);

			String dre = (bible.getVersion() + ": " + bible.getTitle());
			writeVersesATV(file, dre, bible.getAllVerses());
			pW.close();
		} catch (IOException e) {
			e.getMessage();
		}
	}

	/**
	 * Write out the given verses in the ATV format, using the description as
	 * the first line of the file.
	 * 
	 * @param file
	 *            The file that the Bible should be written to.
	 * @param description
	 *            The contents that will be placed on the first line of the
	 *            file, formatted appropriately.
	 * @param verses
	 *            The verses that will be written to the file.
	 */
	public static void writeVersesATV(File file, String description, VerseList verses) {
		try {
			FileWriter outputStream = new FileWriter(file);
			PrintWriter pW = new PrintWriter(outputStream);

			pW.print(description);
			pW.println();

			for (Verse verse : verses) {
				pW.println((verse.getReference().getBook() + "@" + verse.getReference().getChapter() + ":"
						+ verse.getReference().getVerse() + "@" + verse.getText()));
			}
			pW.close();
		} catch (IOException e) {
			e.getMessage();
		}
	}

	/**
	 * Write the string out to the given file. It is presumed that the string is
	 * an HTML rendering of some verses, but really it can be anything.
	 * 
	 * @param file
	 * @param text
	 */
	public static void writeText(File file, String text) {

		try {
			FileWriter outputStream = new FileWriter(file);
			PrintWriter pW = new PrintWriter(outputStream);
			pW.write(text);
			pW.close();
		} catch (IOException e) {
			e.getMessage();
		}
	}

}
