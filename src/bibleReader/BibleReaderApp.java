package bibleReader;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import bibleReader.model.ArrayListBible;
import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.NavigableResults;
import bibleReader.model.Reference;
import bibleReader.model.ResultType;
import bibleReader.model.VerseList;

/**
 * The main class for the Bible Reader Application.
 * 
 * @author cusack
 * @editedBy Nazareth Frezghi and Carmen Rodriguez, 2019.
 */
public class BibleReaderApp extends JFrame {
	public static final int width = 800;
	public static final int height = 600;

	// Main method
	public static void main(String[] args) {
		new BibleReaderApp();
	}

	// Fields
	private BibleReaderModel model;
	private ResultView resultView;
	private JFrame mainFrame;
	public String searchWord;
	private NavigableResults navigableResults;
	int resultNumber;
	int pageNumber;
	int totalPages;
	// Creates a file chooser.
	JFileChooser fc;
	// Creates the buttons
	public JButton searchButton;
	public JButton passageButton;
	public JButton nextButton;
	public JButton previousButton;
	public JTextField input;
	private JTextField resultsNumber;

	/**
	 * Default constructor.
	 */
	public BibleReaderApp() {
		// call the default constructor
		model = new BibleReaderModel();

		// Reads the kjv.atv file
		File kjvFile = new File("kjv.atv");
		VerseList verses = BibleIO.readBible(kjvFile);

		// Reads the esv.atv file
		File esvFile = new File("esv.atv");
		VerseList verses1 = BibleIO.readBible(esvFile);

		// Reads the asv.xmv file
		File asvFile = new File("asv.xmv");
		VerseList verses2 = BibleIO.readBible(asvFile);

		// Creates a new bible with the verses read
		Bible kjv = new ArrayListBible(verses);
		Bible esv = new ArrayListBible(verses1);
		Bible asv = new ArrayListBible(verses2);

		// adds the King James version to the bible
		model.addBible(kjv);
		model.addBible(esv);
		model.addBible(asv);

		// Creates a new resultview using the model
		resultView = new ResultView(model);

		// runs the GUI setup
		setupGUI();
		pack();
		setSize(width, height);

		// So the application exits when you click the "x".
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	/**
	 * Set up the main GUI.
	 */
	private void setupGUI() {
		// Sets the window title
		setTitle("Bible Reader Stage 05");

		// Initializes the JMenu objects and sets their text
		JMenuBar menuBar = new JMenuBar();
		JMenu file = new JMenu("File");
		JMenu help = new JMenu("Help");
		JMenuItem exit = new JMenuItem("Exit");
		JMenuItem about = new JMenuItem("About");
		JMenuItem open = new JMenuItem("Open");

		// Adds the JMenuItem objects to their respective JMenu
		file.add(open);
		file.add(exit);
		help.add(about);

		// Adds the JMenu objects to the menuBar and sets menuBar as the
		// JMenuBar
		menuBar.add(file);
		menuBar.add(help);
		setJMenuBar(menuBar);

		// Initializes the search button and sets its text
		searchButton = new JButton("Search");
		searchButton.setName("SearchButton");
		// Initialize the passage button and sets its text
		passageButton = new JButton("passage");
		passageButton.setName("PassageButton");
		// Initializes the next Button and sets its text.
		nextButton = new JButton("Next");
		nextButton.setName("NextButton");
		nextButton.setEnabled(false);
		// Initializes the previous button and sets its text.
		previousButton = new JButton("Previous");
		previousButton.setName("PreviousButton");
		previousButton.setEnabled(false);

		resultsNumber = new JTextField();
		resultsNumber.setPreferredSize(new Dimension(325, 30));
		// Makes sure the user can't edit the JTextField
		resultsNumber.setEditable(false);
		// Sets the default text to 'No Results'
		resultsNumber.setText("No Results");
		// Centers the text in the JTextField
		resultsNumber.setHorizontalAlignment(JTextField.CENTER);

		// Initializes the pageNumberField JTextField, this shows the user what
		// page their on.
		final JTextField pageNumberField = new JTextField();
		pageNumberField.setPreferredSize(new Dimension(150, 30));
		// Makes sure the user cannot edit the JTextField.
		pageNumberField.setEditable(false);
		// Sets the default text
		pageNumberField.setText("Page 1 ");
		// Center the location of the text field in the JTextField
		pageNumberField.setHorizontalAlignment(JTextField.CENTER);

		// Initializes the input JTextField and sets the size
		input = new JTextField();
		input.setName("InputTextField");
		input.setPreferredSize(new Dimension(300, 30));

		// Initializes the top and bottom JPanels
		JPanel topPanel = new JPanel();
		JPanel bottomPanel = new JPanel();

		// Adds the input and search button to the top panel
		topPanel.add(input);
		topPanel.add(searchButton);
		topPanel.add(passageButton);

		// Adds the results number text field, next button, and previous button
		// to the
		// bottom panel
		bottomPanel.add(resultsNumber, BorderLayout.LINE_START);
		bottomPanel.add(previousButton, BorderLayout.LINE_END);
		bottomPanel.add(nextButton);
		bottomPanel.add(pageNumberField, BorderLayout.LINE_END);

		// Action listener that searches for the word in the input text field
		searchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// gets the text from the input
				searchWord = input.getText();
				ArrayList<Reference> ref = model.getReferencesContainingAllWordsAndPhrases(searchWord);
				navigableResults = new NavigableResults(ref, searchWord, ResultType.SEARCH);
				// sets the search word in the result view so it can search
				resultView.setWordSearchResults(navigableResults);
				// gets the number of found results from the result view
				resultNumber = resultView.getNumberOfResults(navigableResults);
				// sets the text of the resultsNumber to the number of found
				// verses containing the search word
				resultsNumber.setText(
						"There are " + Integer.toString(resultNumber) + " Verses containing the word " + searchWord);
				updateButtons();
			}
		});

		// Action listener that searches for the passage in the input text field
		passageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// gets the text from the input
				searchWord = input.getText();
				ArrayList<Reference> ref = model.getReferencesForPassage(searchWord);
				navigableResults = new NavigableResults(ref, searchWord, ResultType.PASSAGE);
				// sets the search word in the result view so it can search
				resultView.setPassageResults(navigableResults);
				// gets the number of found results from the result view
				resultNumber = resultView.getNumberOfResults(navigableResults);
				// sets the text of the resultsNumber to the number of found
				// verses containing the search word
				resultsNumber.setText("There are " + Integer.toString(resultNumber) + " Verses for " + searchWord);
				updateButtons();
			}
		});

		// Action listener that for the next button that goes to the next page
		// of results.
		nextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//
				// if (navigableResults.hasNextResults() == true) {
				navigableResults.nextResults();
				if (navigableResults.getType() == ResultType.SEARCH) {
					resultView.updateSearchResults();
				} else {
					resultView.updatePassageResults();
				}
				// resultView.setWordSearchResults(navigableResults);
				// gets the number of found results from the result view
				pageNumber = navigableResults.getPageNumber();
				totalPages = navigableResults.getNumberPages();
				// sets the text of the number of pages in total and the one
				// currently
				// displaying
				pageNumberField.setText("Page " + Integer.toString(pageNumber) + " of " + Integer.toString(totalPages));

				updateButtons();
			}
		});

		// Action listener that for the next button that goes to the next page
		// of results.
		previousButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// if (navigableResults.hasNextResults() == true) {
				navigableResults.previousResults();
				// resultView.setWordSearchResults(navigableResults);
				if (navigableResults.getType() == ResultType.SEARCH) {
					resultView.updateSearchResults();
				} else {
					resultView.updatePassageResults();
				}
				// gets the number of found results from the result view
				pageNumber = navigableResults.getPageNumber();
				totalPages = navigableResults.getNumberPages();
				// sets the text of the number of pages in total and the one
				// currently
				// displaying
				pageNumberField.setText("Page " + Integer.toString(pageNumber) + " of " + Integer.toString(totalPages));
				updateButtons();
			}
		});
		// Action listener that closes the program if you press the exit button
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		// Action listener that shows a message dialog with info if you press
		// the about button
		about.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(mainFrame,
						"Created by Prof Cusack, Modified by Nazareth Frezghi and Carmen Rodriguez 2019");
			}
		});

		// Action listener
		open.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openFile();
			}
		});

		// initializes the container that all of the JPanels will be placed on
		Container contents = getContentPane();
		// Adds the top panel to the top and the bottom panel to the bottom
		contents.add(topPanel, BorderLayout.NORTH);
		contents.add(bottomPanel, BorderLayout.SOUTH);

		// Creates a JScrollPane from the resultView and adds it to the center
		JScrollPane scroller = resultView.getScroll();
		contents.add(scroller, BorderLayout.CENTER);

	}

	// Initializes the filechooser
	public void openFile() {
		if (fc == null) {
			fc = new JFileChooser();
		}
		int returnVal = fc.showDialog(mainFrame, "Open");

		// We check whether or not they clicked the "Open" button
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			// We get a reference to the file that the user selected.
			File files = fc.getSelectedFile();
			// Make sure it actually exists.
			if (!files.exists()) {
				showFileNotFoundDialog();
			} else {
				VerseList vl = BibleIO.readBible(files);
				Bible bib = new ArrayListBible(vl);
				model.addBible(bib);
			}
		}
	}

	private void showFileNotFoundDialog() {
		JOptionPane.showMessageDialog(mainFrame, "That file does not exist!", "File not found",
				JOptionPane.ERROR_MESSAGE);
	}

	private void updateButtons() {
		nextButton.setEnabled(navigableResults.hasNextResults());
		previousButton.setEnabled(navigableResults.hasPreviousResults());
	}

}
