package bibleReader.model;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface)
 * @author Nazareth Frezghi (provided the implementation)
 */
public class TreeMapBible implements Bible {

	// The Fields
	private String version;
	private TreeMap<Reference, Verse> theVerses;
	private String title;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param version
	 *            the version of the Bible (e.g. ESV, KJV, ASV, NIV).
	 * @param verses
	 *            All of the verses of this version of the Bible.
	 */
	public TreeMapBible(VerseList verses) {
		theVerses = new TreeMap<Reference, Verse>();
		version = verses.getVersion();
		title = verses.getDescription();

		for (Verse v : verses) {
			Reference r = v.getReference();
			theVerses.put(r, v);
		}
		theVerses.put(new Reference(BookOfBible.Dummy, 1, 1), new Verse(new Reference(BookOfBible.Dummy, 1, 1), ""));
	}

	/**
	 * Returns the number of verses that this Bible is currently storing.
	 * 
	 * @return the number of verses in the Bible.
	 */
	public int getNumberOfVerses() {
		return theVerses.size() - 1;
	}

	/**
	 * Returns a verse list that contains all the verses from the Bible. It
	 * includes version and title.
	 * 
	 * @return a VerseList containing all of the verses from the Bible, in
	 *         order. The version of the VerseList should be set to the version
	 *         of this Bible and the description should be set to the title of
	 *         this Bible.
	 */
	public VerseList getAllVerses() {
		TreeMap<Reference, Verse> vs = new TreeMap<>(theVerses);
		VerseList verseList = new VerseList(getVersion(), getTitle());
		vs.remove(new Reference(BookOfBible.Dummy, 1, 1));
		Set<Map.Entry<Reference, Verse>> mySet = vs.entrySet();
		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, Verse> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue().getText());
			verseList.add(aVerse);
		}
		return verseList;
	}

	/**
	 * Returns which version this object is storing (e.g. ESV, KJV)
	 * 
	 * @return A string representation of the version.
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Returns the title of this version of the Bible.
	 * 
	 * @return A string representation of the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Returns whether or not the reference is actually in the Bible
	 * 
	 * @param ref
	 *            the reference to look up
	 * @return true if and only if ref is actually in this Bible
	 */
	public boolean isValid(Reference ref) {
		boolean temp = false;

		if (ref.getBookOfBible() == BookOfBible.Dummy) {
			return temp;
		}
		if (theVerses.containsKey(ref)) {
			temp = true;
		}
		return temp;
	}

	/**
	 * Returns the text of the verse with the given reference
	 * 
	 * @param ref
	 *            the Reference to the desired verse.
	 * @return A string representation of the text of the verse or null if the
	 *         Reference is invalid.
	 */
	public String getVerseText(Reference r) {
		if (isValid(r)) {
			return theVerses.get(r).getText();
		}
		return null;
	}

	/**
	 * Returns a Verse object for the corresponding Reference.
	 * 
	 * @param ref
	 *            A reference to the desired verse.
	 * @return A Verse object which has Reference r, or null if the Reference is
	 *         invalid.
	 */
	public Verse getVerse(Reference ref) {
		return theVerses.get(ref);
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @param verse
	 *            the verse within the chapter
	 * @return the verse object with reference "book chapter:verse", or null if
	 *         the reference is invalid.
	 */
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		Reference ref = new Reference(book, chapter, verse);
		return theVerses.get(ref);
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for stage 4.
	//
	// For Stage 11 the first two methods below will be implemented as specified
	// in the comments.
	// Do not over think these methods. All three should be pretty
	// straightforward to implement.
	// For Stage 8 (give or take a 1 or 2) you will re-implement them so they
	// work better.
	// At that stage you will create another class to facilitate searching and
	// use it here.
	// (Essentially these two methods will be delegate methods.)
	// ---------------------------------------------------------------------------------------------

	/**
	 * Returns a VerseList of all verses containing <i>phrase</i>, which may be
	 * a word, sentence, or whatever. This method just does simple string
	 * matching, so if <i>phrase</i> is <i>eaten</i>, verses with <i>beaten</i>
	 * will be included.
	 * 
	 * @param phrase
	 *            the word/phrase to search for.
	 * @return a VerseList of all verses containing <i>phrase</i>, which may be
	 *         a word, sentence, or whatever. If there are no such verses,
	 *         returns an empty VerseList. In all cases, the version will be set
	 *         to the version of the Bible (via getVersion()) and the
	 *         description will be set to parameter <i>phrase</i>.
	 */
	public VerseList getVersesContaining(String phrase) {
		TreeMap<Reference, Verse> tMap = new TreeMap<>(theVerses);
		VerseList v = new VerseList(getVersion(), getTitle());
		Set<Map.Entry<Reference, Verse>> mySet = tMap.entrySet();
		// Iterate through the set and put the verses in the VerseList.
		if (!phrase.equals("")) {
			for (Map.Entry<Reference, Verse> element : mySet) {
				if (element.getValue().getText().toLowerCase().contains(phrase.toLowerCase())) {
					Verse aVerse = new Verse(element.getKey(), element.getValue().getText());
					v.add(aVerse);
				}
			}
		}
		return v;
	}

	/**
	 * Returns a ArrayList<Reference> of all references for verses containing
	 * <i>phrase</i>, which may be a word, sentence, or whatever. This method
	 * just does simple string matching, so if <i>phrase</i> is <i>eaten</i>,
	 * verses with <i>beaten</i> will be included.
	 * 
	 * @param phrase
	 *            the phrase to search for
	 * @return a ArrayList<Reference> of all references for verses containing
	 *         <i>phrase</i>, which may be a word, sentence, or whatever. If
	 *         there are no such verses, returns an empty ArrayList<Reference>.
	 */
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		TreeMap<Reference, Verse> tMap = new TreeMap<>(theVerses);
		ArrayList<Reference> r = new ArrayList<Reference>();
		Set<Map.Entry<Reference, Verse>> mySet = tMap.entrySet();
		// Iterate through the set and put the verses in the VerseList.
		if (!phrase.equals("")) {
			for (Map.Entry<Reference, Verse> element : mySet) {
				if (element.getValue().getText().toLowerCase().contains(phrase.toLowerCase())) {
					Verse aVerse = new Verse(element.getKey(), element.getValue().getText());
					r.add(aVerse.getReference());
				}
			}
		}
		return r;
	}

	/**
	 * Returns a verse list
	 * 
	 * @param references
	 *            a ArrayList<Reference> of references for which verses are
	 *            being requested with each element being the Verse with that
	 *            Reference from this Bible.
	 * @return a VerseList with each element being the Verse with that Reference
	 *         from this Bible, or null if the particular Reference does not
	 *         occur in this Bible. Thus, the size of the returned list will be
	 *         the same as the size of the references parameter, with the items
	 *         from each corresponding. The version will be set to the version
	 *         of the Bible (via getVersion()) and the description will be set
	 *         "Arbitrary list of Verses".
	 */
	public VerseList getVerses(ArrayList<Reference> references) {
		VerseList verseList = new VerseList(getVersion(), "Arbitrary list of Verses");
		for (Reference r : references) {
			if (r.getBookOfBible() != null) {
				verseList.add(getVerse(r));

			} else {
				verseList.add(null);
			}
		}
		return verseList;
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for Stage 11.
	//
	// HINT: Do not reinvent the wheel. Some of these methods can be implemented
	// by looking up
	// one or two things and calling another method to do the bulk of the work.
	// ---------------------------------------------------------------------------------------------

	/**
	 * /**
	 * 
	 * @param book
	 *            The book.
	 * @param chapter
	 *            The chapter.
	 * @return the number of the final verse of the given chapter in the given
	 *         book, or -1 if there is a problem (e.g., the book or chapter are
	 *         invalid/null).
	 */
	// @Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		VerseList vList1 = new VerseList(version, title);
		Set<Map.Entry<Reference, Verse>> mySet = theVerses.entrySet();
		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, Verse> element : mySet) {
			if (element.getKey().getChapter() == chapter && element.getKey().getBookOfBible().equals(book)) {
				Verse aVerse = new Verse(element.getKey(), element.getValue().getText());
				vList1.add(aVerse);
			}
		}
		if (vList1.size() != 0) {
			return vList1.get(vList1.size() - 1).getReference().getVerse();

		} else {
			return 0;
		}

	}

	/**
	 * @param book
	 *            The book
	 * @return the number of the final chapter of the given book, or -1 if there
	 *         is a problem (e.g., the book is invalid/null).
	 */
	// @Override
	public int getLastChapterNumber(BookOfBible book) {
		VerseList vList1 = new VerseList(version, title);
		Set<Map.Entry<Reference, Verse>> mySet = theVerses.entrySet();
		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, Verse> element : mySet) {
			if (element.getKey().getBookOfBible().equals(book)) {
				Verse aVerse = new Verse(element.getKey(), element.getValue().getText());
				vList1.add(aVerse);
			}
		}
		if (vList1.size() != 0) {
			return vList1.get(vList1.size() - 1).getReference().getChapter();

		} else {
			return 0;
		}

	}

	/**
	 * @param firstVerse
	 *            the starting verse of the passage
	 * @param lastVerse
	 *            the final verse of the passage
	 * @return a ArrayList<Reference> of all references in this Bible between
	 *         the firstVerse and lastVerse, inclusive of both; or an empty list
	 *         if the range of verses is invalid in this Bible. An invalid range
	 *         includes if either firstVerse or lastVerse is invalid or if
	 *         firstVerse.compareTo(lastVerse) > 0.
	 */
	// @Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> references = new ArrayList<>();
		Reference ref = new Reference(lastVerse.getBookOfBible(), lastVerse.getChapter(), lastVerse.getVerse() + 1);
		if (firstVerse.compareTo(lastVerse) > 0) {
			return references;
		}
		SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, ref);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue().getText());
			references.add(aVerse.getReference());
		}
		return references;
	}

	/**
	 * @param firstVerse
	 *            the starting verse of the passage
	 * @param lastVerse
	 *            the final verse of the passage
	 * @return a ArrayList<Reference> of all references between the firstVerse
	 *         and lastVerse, exclusive of lastVerse; or an empty list if the
	 *         range of verses is invalid in this Bible. An invalid range
	 *         includes if firstVerse is invalid or if
	 *         firstVerse.compareTo(lastVerse) > 0. Notice that lastVerse is
	 *         allowed to be invalid. This can be useful in certain cases to
	 *         find passages more quickly.
	 */
	// @Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> references = new ArrayList<>();
		Reference ref = new Reference(lastVerse.getBookOfBible(), lastVerse.getChapter(), lastVerse.getVerse());
		if (firstVerse.compareTo(lastVerse) > 0) {
			return references;
		}
		SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, ref);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue().getText());
			references.add(aVerse.getReference());
		}
		return references;
	}

	/**
	 * Return a list of all verses from a given book.
	 * 
	 * @param book
	 *            The desired book.
	 * @return a ArrayList<Reference> with version set to this.getVersion(),
	 *         description set to a String representation of <i>book</i>, and
	 *         containing all verses from the given book, in order; or an empty
	 *         list if the book is invalid in this Bible.
	 */
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		if (book != null) {
			return getReferencesInclusive(new Reference(book, 1, 1), new Reference(book, getLastChapterNumber(book),
					getLastVerseNumber(book, getLastChapterNumber(book))));
		} else {
			return new ArrayList<Reference>();
		}
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @return a ArrayList<Reference> containing all references from the given
	 *         chapter of the given book, in order; or an empty list if the
	 *         passage is invalid in this Bible.
	 */
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		return getReferencesExclusive(new Reference(book, chapter, 1), new Reference(book, chapter + 1, 1));
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter1
	 *            the chapter of the book
	 * @param chapter2
	 *            the chapter of the book
	 * @return a ArrayList<Reference> containing all references from the given
	 *         chapters of the given book, in order; or an empty list if the
	 *         passage is invalid in this Bible.
	 */
	// @Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		Reference ref = new Reference(book, chapter1, 1);
		Reference ref2 = new Reference(book, chapter2 + 1, 1);
		return getReferencesExclusive(ref, ref2);
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @param verse1
	 *            the first verse of the chapter
	 * @param verse2
	 *            the second verse of the chapter
	 * @return a ArrayList<Reference> containing all references from the given
	 *         passage, in order; or an empty list if the passage is invalid in
	 *         this Bible.
	 */
	// @Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		Reference ref1 = new Reference(book, chapter, verse1);
		Reference ref2 = new Reference(book, chapter, verse2);

		return getReferencesInclusive(ref1, ref2);
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter1
	 *            the first chapter of the book
	 * @param verse1
	 *            the first verse of the chapter
	 * @param chapter2
	 *            the second chapter of the book
	 * @param verse2
	 *            the second verse of the chapter
	 * @return a ArrayList<Reference> containing all references from the given
	 *         passage, in order; or an empty list if the passage is invalid in
	 *         this Bible.
	 */
	// @Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		Reference ref1 = new Reference(book, chapter1, verse1);
		Reference ref2 = new Reference(book, chapter2, verse2);

		return getReferencesInclusive(ref1, ref2);
	}

	/**
	 * @param firstVerse
	 *            the starting verse of the passage
	 * @param lastVerse
	 *            the final verse of the passage
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representation of the range of verses requested,
	 *         and containing all verses between the firstVerse and lastVerse,
	 *         inclusive of both; or an empty list if the range of verses is
	 *         invalid in this Bible. An invalid range includes if either
	 *         firstVerse or lastVerse is invalid or if
	 *         firstVerse.compareTo(lastVerse) > 0.
	 */
	// @Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		VerseList someVerses = new VerseList(getVersion(), firstVerse + "-" + lastVerse);
		Reference ref = new Reference(lastVerse.getBookOfBible(), lastVerse.getChapter(), lastVerse.getVerse() + 1);
		if (firstVerse.compareTo(lastVerse) > 0) {
			return someVerses;
		}

		SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, ref);
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();
		for (Map.Entry<Reference, Verse> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue().getText());
			someVerses.add(aVerse);
		}
		return someVerses;
	}

	/**
	 * @param firstVerse
	 *            the starting verse of the passage
	 * @param lastVerse
	 *            the final verse of the passage
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representation of the range of verses requested,
	 *         with " excluding the final one" tacked onto the end, and
	 *         containing all verses between the firstVerse and lastVerse,
	 *         exclusive of the last one; or an empty list if the range of
	 *         verses is invalid in this Bible. An invalid range includes if
	 *         firstVerse is invalid or if firstVerse.compareTo(lastVerse) > 0.
	 *         Notice that lastVerse is allowed to be invalid. This can be
	 *         useful in certain cases to find passages more quickly.
	 */
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		VerseList someVerses = new VerseList(getVersion(), firstVerse + "-" + lastVerse);

		// Make sure the references are in the correct order. If not, return an
		// empty list.
		if (firstVerse.compareTo(lastVerse) > 0) {
			return someVerses;
		}
		// Return the portion of the TreeMap that contains the verses between
		// the first and the last, not including the last.
		SortedMap<Reference, Verse> s = theVerses.subMap(firstVerse, lastVerse);
		// Get the entries from the map so we can iterate through them.
		Set<Map.Entry<Reference, Verse>> mySet = s.entrySet();

		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, Verse> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue().getText());
			someVerses.add(aVerse);
		}
		return someVerses;
	}

	/**
	 * Return a list of all verses from a given book.
	 * 
	 * @param book
	 *            The desired book.
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representation of <i>book</i>, and containing all
	 *         verses from the given book, in order; or an empty list if the
	 *         book is invalid in this Bible.
	 */
	// @Override
	public VerseList getBook(BookOfBible book) {
		if (book != null) {
			Reference reference = new Reference(book, 1, 1);
			Reference reference2 = new Reference(book, getLastChapterNumber(book),
					getLastVerseNumber(book, getLastChapterNumber(book)));
			VerseList vList = getVersesInclusive(reference, reference2);
			return vList;
		} else {
			return new VerseList(getVersion(), "");
		}
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representing the book followed by the chapter
	 *         (e.g. "Genesis 1"), and containing all verses from the given
	 *         chapter of the given book, in order; or an empty list if the book
	 *         is invalid in this Bible.
	 */
	// @Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		Reference reference = new Reference(book, chapter, 1);
		Reference reference2 = new Reference(book, chapter, getLastVerseNumber(book, chapter));
		VerseList vList = getVersesInclusive(reference, reference2);
		return vList;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter1
	 *            the chapter of the book
	 * @param chapter2
	 *            the chapter of the book
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representing the book followed by the chapters
	 *         (e.g. "Genesis 1-2"), and containing all verses from the given
	 *         chapters of the given book, in order; or an empty list if the
	 *         passage is invalid in this Bible.
	 */
	// @Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		Reference reference = new Reference(book, chapter1, 1);
		Reference reference2 = new Reference(book, chapter2, getLastVerseNumber(book, chapter2));
		VerseList vList = getVersesInclusive(reference, reference2);
		return vList;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @param verse1
	 *            the first verse of the chapter
	 * @param verse2
	 *            the second verse of the chapter
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representing the verses requested (e.g. "Genesis
	 *         1:3-17"), and containing all verses from the given passage, in
	 *         order; or an empty list if the passage is invalid in this Bible.
	 */
	// @Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		Reference r1 = new Reference(book, chapter, verse1);
		Reference r2 = new Reference(book, chapter, verse2);

		VerseList vList = getVersesInclusive(r1, r2);

		return vList;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter1
	 *            the first chapter of the book
	 * @param verse1
	 *            the first verse of the chapter
	 * @param chapter2
	 *            the second chapter of the book
	 * @param verse2
	 *            the second verse of the chapter
	 * @return a VerseList with version set to this.getVersion(), description
	 *         set to a String representing the verses requested (e.g. "Genesis
	 *         1:3-2:17"), and containing all verses from the given passage, in
	 *         order; or an empty list if the passage is invalid in this Bible.
	 */
	// @Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		Reference r1 = new Reference(book, chapter1, verse1);
		Reference r2 = new Reference(book, chapter2, verse2);

		VerseList vList = getVersesInclusive(r1, r2);

		return vList;
	}
}
