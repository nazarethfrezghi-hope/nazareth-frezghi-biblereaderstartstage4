package bibleReader.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The model of the Bible Reader. It stores the Bibles and has methods for
 * searching for verses based on words or references.
 * 
 * @author cusack
 * @editedBy Nazareth Frezghi and Carmen Rodriguez, 2019
 */
public class BibleReaderModel implements MultiBibleModel {

	// BibleReaderModel fields.

	public static Pattern bookPattern = Pattern.compile("\\s*((?:1|2|3|I|II|III)\\s*\\w+|(?:\\s*[a-zA-Z]+)+)\\s*(.*)");
	public static Pattern pat = Pattern.compile("\\s*((?:1|2|3|I|II|III)\\s*\\w+|(?:\\s*[a-zA-Z]+)+)\\s*(.*)");
	public static String number = "\\s*(\\d+)\\s*";
	public static Pattern cvcvPattern = Pattern.compile(number + ":" + number + "-" + number + ":" + number);
	public static Pattern ccPattern = Pattern.compile(number + "-" + number);
	public static Pattern ccvPattern = Pattern.compile(number + "-" + number + ":" + number);
	public static Pattern cvvPattern = Pattern.compile(number + ":" + number + "-" + number);
	public static Pattern cvPattern = Pattern.compile(number + ":" + number);
	public static Pattern cPattern = Pattern.compile(number);

	TreeMap<String, Bible> bibleMap;
	private HashMap<String, Concordance> concordance;

	/**
	 * BibleReaderModel constructor. Sets up the model.
	 */
	public BibleReaderModel() {
		bibleMap = new TreeMap<String, Bible>();
		concordance = new HashMap<String, Concordance>();

		// for (String s: bibleMap.keySet()){
		// Concordance bib = new Concordance (bibleMap.get(s));
		// concordance.put(s, bib);
		// }
	}

	/**
	 * This method returns the abbreviations for all of the bibles currently
	 * stored.
	 * 
	 * @return an array containing the abbreviations for all of the bibles
	 *         currently stored in the model in alphabetical order.
	 */
	public String[] getVersions() {

		Set<String> vers = bibleMap.keySet();
		String[] versionList = vers.toArray(new String[0]);

		Arrays.sort(versionList);

		return versionList;
	}

	/**
	 * This method returns the number of versions stored in the model.
	 * 
	 * @return the number of versions currently stored in the model.
	 */
	public int getNumberOfVersions() {
		return bibleMap.size();
	}

	/**
	 * This method adds a bible to the ArrayList of bibles.
	 */
	public void addBible(Bible bible) {
		bibleMap.put(bible.getVersion(), bible);
		Concordance c = new Concordance(bible);
		concordance.put(bible.getVersion(), c);
	}

	/**
	 * @param version
	 *            the abbreviation for the version of the Bible that you want.
	 * @return the Bible that has the abbreviation "version", or null if it
	 *         isn't in the model.
	 */
	public Bible getBible(String version) {
		return bibleMap.get(version);
	}

	/**
	 * This method returns a list of all references <i>r</i> such that
	 * <i>words</i> is contained in <i>r</i> for at least one version of the
	 * Bible. In other words, it finds the references for all verses in each
	 * version that contain <i>words</i> and combines the results together so
	 * that a single list of references is returned. The references are in the
	 * order they occur in the Bible and references are only listed once no
	 * matter how many versions had a match for that reference.
	 * 
	 * @param words
	 *            The words to search for.
	 * @return a ArrayList<Reference> containing the results, or an empty list.
	 */
	public ArrayList<Reference> getReferencesContaining(String words) {
		ArrayList<Reference> refList = new ArrayList<>();
		TreeSet<Reference> refs = new TreeSet<>();
		String[] versionList = getVersions();
		for (int i = 0; i < getNumberOfVersions(); i++) {
			String version = versionList[i];
			Bible currentVersion = getBible(version);

			refList = currentVersion.getReferencesContaining(words);
			refs.addAll(refList);
		}
		return new ArrayList<Reference>(refs);
	}

	/**
	 * This method returns the verses from the given Bible for the given list of
	 * references, or null if the Bible isn't in the model.
	 * 
	 * @param version
	 *            The version of the Bible to look in.
	 * @param references
	 *            The set of References we want the full verses for.
	 * @return The verses from the given Bible for the given list of references,
	 *         or null if the Bible isn't in the model. Since this is a delegate
	 *         method, see the getVerses method from the Bible interface for
	 *         details of how the list is constructed.
	 */
	public VerseList getVerses(String version, ArrayList<Reference> references) {

		Bible bibleNeeded = getBible(version);
		VerseList neededVerses = bibleNeeded.getVerses(references);

		return neededVerses;
	}

	// ---------------------------------------------------------------------

	/**
	 * @param version
	 *            The version of the Bible to look in.
	 * @param reference
	 *            The Reference we want the text for from the given version.
	 * @return The text for the given Reference in the given Bible, or the empty
	 *         string if the Reference is not found.
	 */
	public String getText(String version, Reference reference) {
		Bible bible1 = getBible(version);
		if (bible1 != null && bible1.getVerseText(reference) != null) {
			return bible1.getVerseText(reference);
		}
		return "";
	}

	/**
	 * Returns a list of the references for the given passage, in order. If not
	 * all version contain all of the references, return all of the references
	 * in the passage that are in any of the versions. In other words, combine
	 * the lists from all of the versions, keeping any reference that occurs in
	 * any of the versions, and only listing each reference once no matter how
	 * many versions it appears in.
	 * 
	 * @param reference
	 *            A string representation of the reference (e.g. "Genesis
	 *            1:2-3:4")
	 * @return A ArrayList<Reference> containing all of the verses for the given
	 *         passage, in order.
	 */
	public ArrayList<Reference> getReferencesForPassage(String reference) {
		String rest = null;
		String book = null;
		int chap1 = 0;
		int chap2 = 0;
		int verse1 = 0;
		int verse2 = 0;

		BookOfBible bookOB = null;

		Matcher match = bookPattern.matcher(reference);

		if (match.matches()) {
			book = match.group(1);
			rest = match.group(2);
			bookOB = BookOfBible.getBookOfBible(book);
			if (bookOB == null) {
				return new ArrayList<Reference>();
			}
		}
		if (rest.length() == 0) {
			return getBookReferences(bookOB);
		} else if ((match = cvcvPattern.matcher(rest)).matches()) {
			chap1 = Integer.parseInt(match.group(1));
			verse1 = Integer.parseInt(match.group(2));
			chap2 = Integer.parseInt(match.group(3));
			verse2 = Integer.parseInt(match.group(4));
			Reference ref1 = new Reference(bookOB, chap1, verse1);
			Reference ref2 = new Reference(bookOB, chap2, verse2);
			return getPassageReferences(ref1, ref2);

		} else if ((match = ccPattern.matcher(rest)).matches()) {
			chap1 = Integer.parseInt(match.group(1));
			chap2 = Integer.parseInt(match.group(2));
			return getChapterReferences(bookOB, chap1, chap2);

		} else if ((match = ccvPattern.matcher(rest)).matches()) {
			chap1 = Integer.parseInt(match.group(1));
			chap2 = Integer.parseInt(match.group(2));
			verse2 = Integer.parseInt(match.group(3));
			Reference ref1 = new Reference(bookOB, chap1, 1);
			Reference ref2 = new Reference(bookOB, chap2, verse2);
			return getPassageReferences(ref1, ref2);

		} else if ((match = cvvPattern.matcher(rest)).matches()) {
			chap1 = Integer.parseInt(match.group(1));
			verse1 = Integer.parseInt(match.group(2));
			verse2 = Integer.parseInt(match.group(3));
			Reference ref1 = new Reference(bookOB, chap1, verse1);
			Reference ref2 = new Reference(bookOB, chap1, verse2);
			return getPassageReferences(ref1, ref2);

		} else if ((match = cvPattern.matcher(rest)).matches()) {
			chap1 = Integer.parseInt(match.group(1));
			verse1 = Integer.parseInt(match.group(2));
			Reference ref1 = new Reference(bookOB, chap1, verse1);
			return getPassageReferences(ref1, ref1);

		} else if ((match = cPattern.matcher(rest)).matches()) {
			chap1 = Integer.parseInt(match.group(1));
			return getChapterReferences(bookOB, chap1);

		} else {
			ArrayList<Reference> refArrayList = new ArrayList<Reference>();
			return refArrayList;
		}
	}

	// -----------------------------------------------------------------------------
	// -----------------------------------------------------------------------------

	/**
	 * 
	 * @param book
	 *            The book of the bible
	 * @param chapter
	 *            The chapter of the book
	 * @param verse
	 *            The verse of the chapter
	 * @return ref An arrayList of (verse) references
	 */
	public ArrayList<Reference> getVerseReferences(BookOfBible book, int chapter, int verse) {
		TreeSet<Reference> ref = new TreeSet<Reference>();
		ArrayList<Reference> refs = new ArrayList<Reference>();
		for (String bible : bibleMap.keySet()) {
			Verse verse1 = bibleMap.get(bible).getVerse(book, chapter, verse);
			if (verse1 != null) {
				Reference r = new Reference(book, chapter, verse);
				refs.add(r);
			}
		}
		ref = new TreeSet<Reference>(refs);
		return new ArrayList<Reference>(ref);
	}

	/**
	 * 
	 * @param startVerse
	 *            The starting verse reference
	 * @param endVerse
	 *            The end verse reference
	 * @return ref An arrayList of (verse) references inclusive
	 */
	public ArrayList<Reference> getPassageReferences(Reference startVerse, Reference endVerse) {
		TreeSet<Reference> ref = new TreeSet<Reference>();
		for (Bible b : bibleMap.values()) {
			ref.addAll(b.getReferencesInclusive(startVerse, endVerse));
		}
		return new ArrayList<Reference>(ref);
	}

	/**
	 * 
	 * @param book
	 *            The book of the bible
	 * @return ref An arrayList of (verse) references of a book
	 */
	public ArrayList<Reference> getBookReferences(BookOfBible book) {
		TreeSet<Reference> ref = new TreeSet<Reference>();
		for (Bible b : bibleMap.values()) {
			ref.addAll(b.getReferencesForBook(book));
		}
		return new ArrayList<Reference>(ref);
	}

	/**
	 * 
	 * @param book
	 *            The book of the bible
	 * @param chapter
	 *            The chapter of the book
	 * @return ref An arrayList of (verse) references book and chapter specific
	 * 
	 */
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter) {
		TreeSet<Reference> ref = new TreeSet<Reference>();
		for (Bible b : bibleMap.values()) {
			ref.addAll(b.getReferencesForChapter(book, chapter));
		}
		return new ArrayList<Reference>(ref);
	}

	/**
	 * 
	 * @param book
	 *            The book of the bible
	 * @param chapter1
	 *            The first chapter input
	 * @param chapter2
	 *            The second chapter input
	 * @return ref An arrayList of (verse) references book and chapter specific
	 */
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter1, int chapter2) {
		TreeSet<Reference> ref = new TreeSet<Reference>();

		for (Bible b : bibleMap.values()) {
			ref.addAll(b.getReferencesForChapters(book, chapter1, chapter2));
		}
		return new ArrayList<Reference>(ref);
	}

	/**
	 * 
	 * @param book
	 *            The book of the bible
	 * @param chapter
	 *            The chapter of the book
	 * @param verse1
	 *            The verse1 of the input
	 * @param verse2
	 *            The verse2 of the input
	 * @return ref An arrayList of (verse) references book and chapter specific
	 */
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter, int verse1, int verse2) {
		TreeSet<Reference> ref = new TreeSet<Reference>();

		for (Bible b : bibleMap.values()) {
			ref.addAll(b.getReferencesForPassage(book, chapter, verse1, verse2));
		}
		return new ArrayList<Reference>(ref);
	}

	/**
	 * 
	 * @param book
	 *            The book bible
	 * @param chapter1
	 *            The chapter book
	 * @param verse1
	 *            The verse1 input
	 * @param chapter2
	 *            The chapter2 input
	 * @param verse2
	 *            The verse2 input
	 * @return ref An arrayList of (verse) references book and chapter specific
	 */
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		TreeSet<Reference> ref = new TreeSet<Reference>();

		for (Bible b : bibleMap.values()) {
			ref.addAll(b.getReferencesForPassage(book, chapter1, verse1, chapter2, verse2));
		}
		return new ArrayList<Reference>(ref);
	}

	// ------------------------------------------------------------------
	// These are the better searching methods - STAGE 12.
	//

	public ArrayList<Reference> getReferencesContainingWord(String word) {
		TreeSet<Reference> catalog = new TreeSet<Reference>();
		for (String key : bibleMap.keySet()) {
			Concordance con = concordance.get(key);
			catalog.addAll(con.getReferencesContaining(word));
		}
		return new ArrayList<Reference>(catalog);
	}

	public ArrayList<Reference> getReferencesContainingAllWords(String words) {
		words = words.replace(",", "");
		String[] word = words.split(" ");
		ArrayList<String> everyWord = new ArrayList<String>();
		for (String w : word) {
			if (!everyWord.contains(w.toLowerCase())) {
				everyWord.add(w);
			}
		}
		TreeSet<Reference> refList = new TreeSet<Reference>();
		for (String key : bibleMap.keySet()) {
			Concordance con = concordance.get(key);
			refList.addAll(con.getReferencesContainingAll(everyWord));
		}
		return new ArrayList<Reference>(refList);
	}

	public ArrayList<Reference> getReferencesContainingAllWordsAndPhrases(String words) {
		int j = 0;
		int firstWord = 0;
		int nextWord = 0;
		int count = 0;
		ArrayList<String> phrases = new ArrayList<>();
		ArrayList<Reference> ret = new ArrayList<>();

		for (int i = 0; i < words.length(); i++) {
			if (words.substring(i, i + 1).equals("\"")) {
				count++;
			}
		}
		if (count == 1) {
			words = words.replace("\"", "");
		}
		if (words.contains("\"")) {
			while (j < words.lastIndexOf("\"")) {
				firstWord = words.indexOf("\"", j) + 1;
				nextWord = words.indexOf("\"", firstWord + 1);
				if (nextWord != -1) {// if its out of bounds then...
					if (!words.substring(firstWord, nextWord).equals(" ")
							&& !words.substring(firstWord, nextWord).equals("")) {
						phrases.add(words.substring(firstWord, nextWord));
						j = nextWord;
					}
					j = nextWord;
				} else {
					j = words.lastIndexOf("\"");
				}

			}
			for (String phrase : phrases) {
				String secString = phrase.toLowerCase().trim();
				ArrayList<Reference> phraseRefs = getReferencesContainingAllWords(secString);
				TreeSet<Reference> tempRefs = new TreeSet<Reference>();
				for (String key : bibleMap.keySet()) {
					for (Reference reference : phraseRefs) {
						String firString = getText(key, reference).toLowerCase().trim();
						if (firString.contains(secString)) {
							tempRefs.add(reference);
						}
					}
				}
				phraseRefs.retainAll(tempRefs);

				if (ret.size() == 0) {
					ret.addAll(phraseRefs);
				} else {
					ret.retainAll(phraseRefs);
				}
			}
			for (String phrase : phrases) {
				words = words.toLowerCase().trim();
				phrase = phrase.toLowerCase().trim();
				words = words.replace(phrase, "");
				words = words.replaceAll("\"", "");
			}
			ArrayList<Reference> indWords = getReferencesContainingAllWords(words);
			if (indWords.size() != 0) {
				ret.retainAll(indWords);
			}
		} else {
			return getReferencesContainingAllWords(words);
		}
		return ret;
	}
}
