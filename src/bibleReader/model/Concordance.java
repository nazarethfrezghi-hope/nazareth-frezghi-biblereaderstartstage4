package bibleReader.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Concordance is a class which implements a concordance for a Bible. In other
 * words, it allows the easy lookup of all references which contain a given
 * word.
 * 
 * @author Chuck Cusack, March 2013 (Provided the interface)
 * @author ?, March 2013 (Provided the implementation details)
 */
public class Concordance {
	// Add fields here. (I actually only needed one field.)
	// key string (every word in the bible is a key - value is and ArrayList of
	// references
	private HashMap<String, ArrayList<Reference>> references;

	/**
	 * Construct a concordance for the given Bible.
	 */
	public Concordance(Bible bible) {
		VerseList v1 = bible.getAllVerses();
		references = new HashMap<String, ArrayList<Reference>>();
		for (Verse v : v1) {
			ArrayList<String> words = extractWords(v.getText().toLowerCase());
			for (String word : words) {
				if (references.get(word) == null) {
					ArrayList<Reference> r = new ArrayList<Reference>();
					r.add(v.getReference());
					references.put(word, r);
				} else {
					Reference ref2 = references.get(word).get(references.get(word).size() - 1);
					if (!ref2.equals(v.getReference())) {
						references.get(word).add(v.getReference());
					}
				}
			}
		}
	}

	/**
	 * Return the list of references to verses that contain the word 'word'
	 * (ignoring case) in the version of the Bible that this concordance was
	 * created with.
	 * 
	 * @param word
	 *            a single word (no spaces, etc.)
	 * @return the list of References of verses from this version that contain
	 *         the word, or an empty list if no verses contain the word.
	 */
	public ArrayList<Reference> getReferencesContaining(String word) {
		String newWord = word.toLowerCase();
		if (!newWord.equals(" ")) {
			if (references.containsKey(newWord)) {
				return references.get(newWord);
			}
		}
		return new ArrayList<>();
	}

	/**
	 * Given an array of Strings, where each element of the array is expected to
	 * be a single word (with no spaces, etc., but ignoring case), return a
	 * ArrayList<Reference> containing all of the verses that contain <i>all of
	 * the words</i>.
	 * 
	 * @param words
	 *            A list of words.
	 * @return An ArrayList<Reference> containing references to all of the
	 *         verses that contain all of the given words, or an empty list if
	 */
	public ArrayList<Reference> getReferencesContainingAll(ArrayList<String> words) {
		ArrayList<Reference> refList = new ArrayList<Reference>();
		TreeSet<Reference> refs = new TreeSet<Reference>();
		ArrayList<Reference> refs2 = references.get(words.get(0).toLowerCase());
		if (refs2 != null) {
			refList.addAll(refs2);
		}
		for (int i = 1; i < words.size(); i++) {
			String word = words.get(i).toLowerCase();
			refs = new TreeSet<Reference>(getReferencesContaining(word));
			refList.retainAll(refs);
		}
		return refList;
	}
//	public ArrayList<Reference> getReferencesContainingAll(String phrase) {
//		ArrayList<Reference> r = new ArrayList<Reference>();
//		for (Verse verse : verses) {
//			if (!phrase.equals("")) {
//				if (verse.getText().toLowerCase().contains(phrase.toLowerCase())) {
//					r.add(verse.getReference());
//				}
//			}
//		}
//		return r;
//	}

	public static ArrayList<String> extractWords(String text) {
		text = text.toLowerCase();
		// Removes a few HTML tags (relevant to ESV) and 's at the end of words.
		// Replaces them with space space so words around them don't get
		// squiahed
		// together. Notice the two types of apostrophes each is used in a
		// different version.
		text = text.replaceAll("(<sup>[,\\w]*?</sup>|'s|�s|&#\\w*;|,)", " ");
		text = text.replaceAll(",", "");
		String[] words = text.split("\\W+");
		ArrayList<String> toRet = new ArrayList<String>(Arrays.asList(words));
		toRet.remove("");
		return toRet;
	}
}
